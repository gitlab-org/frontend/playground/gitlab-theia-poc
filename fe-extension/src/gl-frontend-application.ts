import { injectable } from 'inversify';
import { FrontendApplication } from '@theia/core/lib/browser/frontend-application';

@injectable()
export class GlFrontendApplication extends FrontendApplication {
  /** @override */ protected async restoreLayout(): Promise<boolean> {
    return Promise.resolve(false);
  }
}
