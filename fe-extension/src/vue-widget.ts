import Vue, { VueConstructor, CreateElement } from 'vue';
import { injectable } from 'inversify';
import { DisposableCollection, Disposable } from '@theia/core/lib/common';
import { BaseWidget, Message } from '@theia/core/lib/browser/widgets/widget';

@injectable()
export class VueWidget extends BaseWidget {

    private _component: VueConstructor;
    private _instance: Vue | null;
    private _root: Element | undefined;
    protected readonly onRender = new DisposableCollection();

    constructor(component: VueConstructor) {
        super();
        this._component = component;
        this._instance = null;
        this.toDispose.push(Disposable.create(() => { this.disposeWidget() }));
        this.scrollOptions = {
            suppressScrollX: true,
            minScrollbarLength: 35
        };
    }

    protected onUpdateRequest(msg: Message): void {
        super.onUpdateRequest(msg);

        this.disposeWidget();
        this.setupWidget();
    }

    protected createVueInstanceOptions(): object {
        return {
            el: this._root,
            render: (h: CreateElement) => h(this._component, []),
        };
    }

    private disposeWidget(): void {
        this.disposeVue();
        this.disposeRoot();
    }

    private setupWidget(): void {
        this.setupRoot();
        this.setupVue();
    }

    private setupRoot(): void {
        this._root = document.createElement('div');
        this.node.appendChild(this._root);
    }

    private setupVue(): void {
        this._instance = new Vue(this.createVueInstanceOptions());
    }

    private disposeVue(): void {
        if (!this._instance) {
            return;
        }

        this._instance.$destroy();
        this._instance = null;
    }

    private disposeRoot(): void {
        if (!this._root) {
            return;
        }

        this._root.remove();
        this._root = undefined;
    }
}
