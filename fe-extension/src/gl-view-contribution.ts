import { injectable } from 'inversify';
import { AbstractViewContribution } from '@theia/core/lib/browser/shell/view-contribution';
import { FrontendApplicationContribution, FrontendApplication } from '@theia/core/lib/browser/frontend-application';
import { TabBarToolbarContribution, TabBarToolbarRegistry } from '@theia/core/lib/browser/shell/tab-bar-toolbar';
import { GlWidget } from './gl-widget';
import { GL_WIDGET_ID } from './constants';

@injectable()
export class GlViewContribution extends AbstractViewContribution<GlWidget> implements FrontendApplicationContribution, TabBarToolbarContribution {
  constructor() {
    super({
      widgetId: GL_WIDGET_ID,
      widgetName: 'GitLab',
      defaultWidgetOptions: {
        area: 'right',
        rank: 400,
      },
      toggleCommandId: 'gitlabView:toggle',
      toggleKeybinding: 'ctrlcmd+shift+g',
    });
  }

  async initializeLayout(app: FrontendApplication): Promise<void> {
    await this.openView();
  }

  registerToolbarItems(registry: TabBarToolbarRegistry): void {
    // noop
  }
}
