import { inject, named } from 'inversify';
import { GitWatcher, GitStatusChangeEvent } from '@theia/git/lib/common';
import { VueWidget } from "./vue-widget";
import LoremIpsum from '../vue_components/lorem-ipsum.vue';
import { GL_WIDGET_ID } from './constants';
import { GL_STORE, GlStore } from './store';
import Vuex from 'vuex';

export type GlWidgetFactory = () => GlWidget;
export const GlWidgetFactory = Symbol('GlWidgetFactory');

export class GlWidget extends VueWidget {
  constructor(
    @inject(GitWatcher) protected readonly gitWatcher: GitWatcher,
    @inject(Vuex.Store) @named(GL_STORE) protected readonly store: GlStore,
  ) {
    super(LoremIpsum)

    this.id = GL_WIDGET_ID;
    this.title.label = 'GitLab - Lorem Ipsum';
    this.title.caption = "It's a me! GitLab!";
    this.title.iconClass = 'fa fa-gitlab';
    this.addClass('gl-theia-widget');

    this.toDispose.push(this.gitWatcher.onGitEvent(e => this.onGitEvent(e)));

    this.update();
  }

  protected onGitEvent({ status, oldStatus }: GitStatusChangeEvent) {
    const { branch } = status;
    const { branch: oldBranch} = oldStatus || {};

    if (branch !== oldBranch) {
      this.onBranchChange(branch || '');
    }
  }

  protected onBranchChange(branch: string) {
    this.store.dispatch('setBranch', branch);
  }

  protected createVueInstanceOptions() {
    return {
      ...super.createVueInstanceOptions(),
      store: this.store,      
    };
  }
}
