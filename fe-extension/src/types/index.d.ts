declare module '*.useable.css' {
  export function use(): void;
  export function unuse(): void;
}
