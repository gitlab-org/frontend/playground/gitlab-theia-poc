import { ContainerModule } from 'inversify';
import Vue from 'vue';
import Vuex from 'vuex';
import { WorkspaceFrontendContribution } from '@theia/workspace/lib/browser';
import { GlViewContribution } from './gl-view-contribution';
import { bindViewContribution, WidgetFactory, FrontendApplicationContribution, FrontendApplication } from '@theia/core/lib/browser';
import { TabBarToolbarContribution } from '@theia/core/lib/browser/shell/tab-bar-toolbar';
import { ThemingCommandContribution } from '@theia/core/lib/browser/theming';
import { GlViewService } from './gl-view-service';
import { GlWidgetFactory, GlWidget } from './gl-widget';
import { GlFrontendApplication } from './gl-frontend-application';
import createStore, { GlStore, GL_STORE } from './store';
import { NoopThemingCommandContribution } from './theia-overrides/noop-theming-command-contribution';
import { NoopWorkspaceFrontendContribution } from './theia-overrides/noop-workspace-frontend-contribution';

import '@gitlab/ui/dist/index.css';
import '../assets/styles/variables.css';
import '../assets/styles/index.css';
import '../assets/styles/utilities.css';

Vue.use(Vuex);

export default new ContainerModule((bind, unbind, isBound, rebind) => {
  bind(GlWidget).toSelf();
  bind(GlWidgetFactory).toFactory((ctx) => () => {
    return ctx.container.resolve<GlWidget>(GlWidget);
  });

  bind(GlViewService).toSelf().inSingletonScope();
  bind(WidgetFactory).toService(GlViewService);

  bindViewContribution(bind, GlViewContribution);
  bind(FrontendApplicationContribution).toService(GlViewContribution);
  bind(TabBarToolbarContribution).toService(GlViewContribution);

  rebind(FrontendApplication).to(GlFrontendApplication);
  rebind(ThemingCommandContribution).to(NoopThemingCommandContribution);
  rebind(WorkspaceFrontendContribution).to(NoopWorkspaceFrontendContribution);

  // NOTE: 
  // The following line doesn't actually remove "outline" from the view...
  // I think it's because it's part of the default layout :(
  // 
  // rebind(OutlineViewContribution).to(NoopOutlineViewContribution);

  bind<GlStore>(Vuex.Store).toDynamicValue(createStore).inSingletonScope().whenTargetNamed(GL_STORE);
});
