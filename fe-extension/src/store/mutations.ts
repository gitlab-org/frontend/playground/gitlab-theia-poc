import * as types from './mutation_types';
import { GlStoreState } from '.';

export default {
  [types.SET_BRANCH](state : GlStoreState, branchName : string) {
    state.branch = branchName;
  }
};
