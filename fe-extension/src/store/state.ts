export interface GlStoreState {
  branch: string,
};

export default () : GlStoreState => ({
  branch: 'master',
});
