import { Store } from 'vuex';
import * as actions from './actions';
import mutations from './mutations';
import createState, { GlStoreState } from './state';

export { GlStoreState };

export const GL_STORE = 'gl-store';

export type GlStore = Store<GlStoreState>;

export default () => new Store<GlStoreState>({
  actions,
  mutations,
  state: createState(),
})
