import * as types from './mutation_types';

export const setBranch = (ctx : any, branchName : string) => {
  ctx.commit(types.SET_BRANCH, branchName);  
}
