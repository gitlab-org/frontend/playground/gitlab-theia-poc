import { ThemingCommandContribution } from '@theia/core/lib/browser/theming';
import { CommandRegistry, MenuModelRegistry } from '@theia/core';

export class NoopThemingCommandContribution extends ThemingCommandContribution {
  registerCommands(commands: CommandRegistry): void {
    // do nothing
  }

  registerMenus(menus: MenuModelRegistry): void {
    // do nothing
  }
}
