import { OutlineViewContribution } from '@theia/outline-view/lib/browser/outline-view-contribution';
import { TabBarToolbarRegistry } from '@theia/core/lib/browser/shell/tab-bar-toolbar';
import { CommandRegistry, MenuModelRegistry } from '@theia/core';
import { KeybindingRegistry } from '@theia/core/lib/browser';

export class NoopOutlineViewContribution extends OutlineViewContribution {
  registerCommands(commands: CommandRegistry): void {
    // noop
  }

  registerToolbarItems(toolbar: TabBarToolbarRegistry): void {
    // noop
  }

  registerMenus(menus: MenuModelRegistry): void {
    // noop
  }

  registerKeybindings(keybindings: KeybindingRegistry): void {
    // noop
  }
}
