import { WorkspaceFrontendContribution } from '@theia/workspace/lib/browser';
import { CommandRegistry, MenuModelRegistry } from '@theia/core';
import { KeybindingRegistry } from '@theia/core/lib/browser';

export class NoopWorkspaceFrontendContribution extends WorkspaceFrontendContribution {
    registerCommands(commands: CommandRegistry): void {
      // noop
    }

    registerMenus(menus: MenuModelRegistry): void {
      // noop
    }

    registerKeybindings(keybindings: KeybindingRegistry): void {
      // noop
    }
}
