import { injectable, inject } from 'inversify';
import { MaybePromise } from '@theia/core';
import { WidgetFactory } from '@theia/core/lib/browser';
import { BaseWidget } from '@theia/core/lib/browser/widgets'
import { GL_WIDGET_ID } from './constants';
import { GlWidgetFactory } from './gl-widget';

@injectable()
export class GlViewService implements WidgetFactory {
  id = GL_WIDGET_ID;  

  constructor(@inject(GlWidgetFactory) protected factory: GlWidgetFactory) { }

  createWidget(options?: any): MaybePromise<BaseWidget> {
    return Promise.resolve(this.factory());
  }
}
