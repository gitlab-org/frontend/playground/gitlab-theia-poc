const genConfig = require("./webpack.config.js");
const merge = require("webpack-merge");
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = merge.smart(genConfig, {
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: "vue-loader"
      }
    ]
  },
  plugins: [new VueLoaderPlugin()]
});
